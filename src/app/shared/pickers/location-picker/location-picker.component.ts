import { PlaceLocation, Coordinates } from './../../../places/location.model';
import { map, switchMap } from 'rxjs/operators';
import { MapModalComponent } from './../../modals/map-modal/map-modal.component';
import {
  ModalController,
  ActionSheetController,
  AlertController
} from '@ionic/angular';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Plugins, Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss']
})
export class LocationPickerComponent implements OnInit {
  selectedLocationImage: string;
  isLoading = false;
  @Output() locationPick = new EventEmitter<PlaceLocation>();

  constructor(
    private modalController: ModalController,
    private httpClient: HttpClient,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  onPickLocation() {
    this.actionSheetController
      .create({
        header: 'Please Choose',
        buttons: [
          { text: 'Auto-Locate', handler: () => this.locateUser() },
          { text: 'Pick on Map', handler: () => this.openMap() },
          { text: 'Cancel', role: 'cancel' }
        ]
      })
      .then(actionElement => {
        actionElement.present();
      });
  }

  private showErrorAlert() {
    this.alertController
      .create({
        header: 'Could not fetch location!',
        message: 'Please use the map to pick location.',
        buttons: ['Okay']
      })
      .then(alertElement => {
        alertElement.present();
      });
  }

  private locateUser() {
    // console.log('Locate User');
    if (!Capacitor.isPluginAvailable('Geolocation')) {
      this.showErrorAlert();
      return;
    }

    this.isLoading = true;
    Plugins.Geolocation.getCurrentPosition()
      .then(geoPosition => {
        const coordinates: Coordinates = {
          lat: geoPosition.coords.latitude,
          lng: geoPosition.coords.longitude
        };

        this.createPlace(coordinates.lat, coordinates.lng);
        this.isLoading = false;
      })
      .catch(err => {
        this.isLoading = false;
        this.showErrorAlert();
      });
  }

  private openMap() {
    this.modalController
      .create({
        id: 'mapModal',
        component: MapModalComponent
      })
      .then(modalElement => {
        modalElement.onDidDismiss().then(modalData => {
          // console.log(modalData);
          if (modalData.role !== 'coord-selected') {
            return;
          }
          // const pickedLocation: PlaceLocation = {
          //   lat: modalData.data.lat,
          //   lng: modalData.data.lng,
          //   address: null,
          //   staticMapImageUrl: null
          // };
          // this.getAddress(modalData.data.lat, modalData.data.lng).subscribe(address => {
          //   console.log(address);
          // });

          const coordinates: Coordinates = {
            lat: modalData.data.lat,
            lng: modalData.data.lng
          };

          this.createPlace(coordinates.lat, coordinates.lng);
        });
        modalElement.present();
      });
  }

  private getAddress(lat: number, lng: number) {
    return this.httpClient
      .get<any>(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyB3NPXarmQqg2JXQpYu5ENdzkDOG-w8PLw`
      )
      .pipe(
        map(geoData => {
          if (!geoData || !geoData.results || geoData.results.length === 0) {
            return null;
          }

          return geoData.results[0].formatted_address;
        })
      );
  }

  private createPlace(lat: number, lng: number) {
    const pickedLocation: PlaceLocation = {
      lat,
      lng,
      address: null,
      staticMapImageUrl: null
    };

    this.isLoading = true;
    this.getAddress(lat, lng)
      .pipe(
        switchMap(address => {
          pickedLocation.address = address;
          return of(
            this.getMapImage(pickedLocation.lat, pickedLocation.lng, 14)
          );
        })
      )
      .subscribe(staticMapImageUrl => {
        pickedLocation.staticMapImageUrl = staticMapImageUrl;
        // console.log(staticMapImageUrl);
        this.selectedLocationImage = staticMapImageUrl;
        this.isLoading = false;

        this.locationPick.emit(pickedLocation);
      });
  }

  private getMapImage(lat: number, lng: number, zoom: number) {
    return `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=${zoom}&size=500x300&maptype=roadmap
    &markers=color:red%7Clabel:Place%7C${lat},${lng}
    &key=AIzaSyB3NPXarmQqg2JXQpYu5ENdzkDOG-w8PLw`;
  }
}
