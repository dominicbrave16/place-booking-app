import { Platform } from '@ionic/angular';
import {
  Plugins,
  Capacitor,
  CameraSource,
  CameraResultType
} from '@capacitor/core';
import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss']
})
export class ImagePickerComponent implements OnInit {
  @ViewChild('filePicker', { static: false }) filePicker: ElementRef<
    HTMLInputElement
  >;
  selectedImage: string;
  @Output() imagePick = new EventEmitter<string | File>();
  usePicker = false;

  constructor(private platform: Platform) {}

  ngOnInit() {
    // console.log('Mobile: ', this.platform.is('mobile'));
    // console.log('Hybrid: ', this.platform.is('hybrid')); // Use this as an indicator
    // console.log('IOS: ', this.platform.is('ios'));
    // console.log('Android: ', this.platform.is('android'));
    // console.log('Desktop: ', this.platform.is('desktop'));

    if (
      (this.platform.is('mobile') && !this.platform.is('hybrid')) ||
      this.platform.is('desktop')
    ) {
      this.usePicker = true;
    }
  }

  onPickImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.filePicker.nativeElement.click();
      return;
    }

    // Plugins.Camera.getPhoto({
    //   quality: 50,
    //   source: CameraSource.Prompt,
    //   correctOrientation: true,
    //   height: 320,
    //   width: 200,
    //   resultType: CameraResultType.Base64
    // })
    //   .then(image => {
    //     this.selectedImage = image.base64String;
    //     this.imagePick.emit(this.selectedImage);
    //   })
    //   .catch(err => {
    //     console.log(err);
    //     if (this.usePicker) {
    //       this.filePicker.nativeElement.click();
    //     }
    //     return false;
    //   });

    Plugins.Camera.getPhoto({
      quality: 50,
      source: CameraSource.Prompt,
      correctOrientation: true,
      height: 320,
      width: 200,
      resultType: CameraResultType.DataUrl
    })
      .then(image => {
        this.selectedImage = image.dataUrl;
        this.imagePick.emit(this.selectedImage);
      })
      .catch(err => {
        console.log(err);
        if (this.usePicker) {
          this.filePicker.nativeElement.click();
        }
        return false;
      });
  }

  onFileChoosen(event: Event) {
    const pickedFile = (event.target as HTMLInputElement).files[0];


    if (!pickedFile) {
      return;
    }

    const fileReader = new FileReader();

    fileReader.onload = () => {
      const dataUrl = fileReader.result.toString();
      this.selectedImage = dataUrl;

      console.log('Picked File', pickedFile);

      this.imagePick.emit(pickedFile);
    };

    fileReader.readAsDataURL(pickedFile);
  }
}
