import { ImagePickerComponent } from './pickers/image-picker/image-picker.component';
import { CommonModule } from '@angular/common';
import { MapModalComponent } from './modals/map-modal/map-modal.component';
import { LocationPickerComponent } from './pickers/location-picker/location-picker.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [LocationPickerComponent, MapModalComponent , ImagePickerComponent],
    imports: [IonicModule, CommonModule],
    exports: [LocationPickerComponent, MapModalComponent, ImagePickerComponent],
    entryComponents: [MapModalComponent]
})
export class SharedModule { }
