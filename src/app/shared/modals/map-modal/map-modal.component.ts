import { environment } from './../../../../../../ionic-angular/src/environments/environment.prod';
import { ModalController, LoadingController } from '@ionic/angular';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Renderer2,
  OnDestroy,
  Input
} from '@angular/core';

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss']
})
export class MapModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('googleMap', { static: false }) mapElement: ElementRef;
  @Input() center = { lat: 10.328801, lng: 123.907522 };
  @Input() selectable = true;
  @Input() closeButtonText = 'Cancel';
  @Input() title = 'Pick Location';
  mapClickListener: any;
  googleMaps: any;

  constructor(
    private modalController: ModalController,
    private render2: Renderer2,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.loadingController
      .create({ message: 'Loading...' })
      .then(loadingElement => {
        loadingElement.present();

        this.getGoogleMaps()
          .then(googleMaps => {
            this.googleMaps = googleMaps;
            const mapElement = this.mapElement.nativeElement;
            const gMap = new googleMaps.Map(mapElement, {
              center: this.center,
              zoom: 16
            });

            googleMaps.event.addListenerOnce(gMap, 'idle', () => {
              this.render2.addClass(mapElement, 'visible');
              loadingElement.dismiss();
            });

            if (this.selectable) {
              // Inorder to prevent memory leak
              this.mapClickListener = gMap.addListener('click', event => {
                const selectedCoordinates = {
                  lat: event.latLng.lat(),
                  lng: event.latLng.lng()
                };

                this.modalController.dismiss(
                  selectedCoordinates,
                  'coord-selected',
                  'mapModal'
                );
              });
            } else {
              const marker = new googleMaps.Marker({
                position: this.center,
                map: gMap,
                title: 'Location'
              });
              marker.setMap(gMap);
            }

          })
          .catch(error => {
            console.log(error);
          });
      });
  }

  onCancel() {
    this.modalController.dismiss({}, 'cancel', 'mapModal');
  }

  private getGoogleMaps(): Promise<any> {
    const win = window as any;
    const googleModule = win.google;

    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    }

    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src =
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyB3NPXarmQqg2JXQpYu5ENdzkDOG-w8PLw';
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const loadedGoogleModule = win.google;
        if (loadedGoogleModule && loadedGoogleModule.maps) {
          resolve(loadedGoogleModule.maps);
        } else {
          reject('Google Maps SDK not available.');
        }
      };
    });
  }

  ngOnDestroy() {
    // To prevent memory leak
    if (this.mapClickListener) {
      this.googleMaps.event.removeListener(this.mapClickListener);
    }
  }
}
