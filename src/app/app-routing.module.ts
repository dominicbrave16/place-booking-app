import { AuthGuard } from './auth/auth.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'places', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthPageModule)
  },
  {
    path: 'places',
    loadChildren: () =>
      import('./places/places.module').then(m => m.PlacesPageModule),
    // canActivate: [AuthGuard]
    canLoad: [AuthGuard]
  },
  {
    path: 'bookings',
    loadChildren: () =>
      import('./bookings/bookings.module').then(m => m.BookingsPageModule),
    // canActivate: [AuthGuard]
    canLoad: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    // { preloadingStrategy: PreloadAllModules } - loads all remaining modules after loading the necsesary modules
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
