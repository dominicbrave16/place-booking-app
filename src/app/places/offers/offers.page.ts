import { Router } from '@angular/router';
import { Place } from './../place.model';
import { PlacesService } from './../places.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonItemSliding, LoadingController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit, OnDestroy {
  offers: Place[];
  private placesSubscription: Subscription;
  isLoading = false;

  constructor(
    private placeService: PlacesService,
    private router: Router,
    // private loadingController: LoadingController
  ) { }

  ngOnInit() {
    // this.offers = this.placeService.places; // Replaced with an observable

    // For cleaning subscription
    this.placesSubscription = this.placeService.places.subscribe(
      places => {
        this.offers = places;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('Ion View Will Enter');

    // Better to use Subjects... *****
    // this.offers = this.placeService.places;
    // this.loadingController.create({
    //   spinner: 'dots'
    // }).then(loadingElement => {
    //   loadingElement.present();
    //   this.placeService.fetchPlaces().subscribe(() => {
    //     loadingElement.dismiss();
    //   });
    // });

    // this.placeService.fetchPlaces().subscribe();
    if (this.offers.length <= 0) {
      this.isLoading = true;
      this.placeService.fetchPlaces().subscribe(() => {
        this.isLoading = false;
      });
      }
  }

  // ionViewDidEnter() {
  //   console.log('Ion View Did Enter');
  // }

  // ionViewWillLeave() {
  //   console.log('Ion View Will Leave');
  // }

  // ionViewDidLeave() {
  //   console.log('Ion View Did Leave');
  // }

  ngOnDestroy() {
    // console.log('On Destroy');
    if (this.placeService) {
      this.placesSubscription.unsubscribe();
    }
  }

  onEdit(offerId: string, slidingItem: IonItemSliding) {
    slidingItem.close();
    this.router.navigate(['/', 'places', 'tabs', 'offers', 'edit', offerId]);
  }
}

