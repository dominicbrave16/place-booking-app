import { PlaceLocation } from './../../location.model';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PlacesService } from './../../places.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

function base64toBlob(base64Data, contentType) {
  contentType = contentType || '';
  const sliceSize = 1024;
  const byteCharacters = atob(base64Data);
  const bytesLength = byteCharacters.length;
  const slicesCount = Math.ceil(bytesLength / sliceSize);
  const byteArrays = new Array(slicesCount);

  for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
    const begin = sliceIndex * sliceSize;
    const end = Math.min(begin + sliceSize, bytesLength);

    const bytes = new Array(end - begin);
    for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
      bytes[i] = byteCharacters[offset].charCodeAt(0);
    }
    byteArrays[sliceIndex] = new Uint8Array(bytes);
  }
  return new Blob(byteArrays, { type: contentType });
}

@Component({
  selector: 'app-new-offer',
  templateUrl: './new-offer.page.html',
  styleUrls: ['./new-offer.page.scss']
})
export class NewOfferPage implements OnInit {
  offerForm: FormGroup;

  constructor(
    private placesService: PlacesService,
    private router: Router,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.offerForm = new FormGroup({
      titleFormCtrl: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      desctriptionFormCtrl: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.maxLength(180)] // Maximum length allowed
      }),
      priceFormCtrl: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(1)] // Should always be equal or greater than 1
      }),
      dateFromFormCtrl: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      dateToFormCtrl: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      locationFormCtrl: new FormControl(null, {
        validators: [Validators.required]
      }),
      imageFormCntrl: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }

  onCreateOffer() {
    // Checks is form is valid or ther is no image added
    if (!this.offerForm.valid || !this.offerForm.get('imageFormCntrl').value) {
      return;
    }

    // console.log('Creating offer...');
    // console.log(this.offerForm.value);

    this.loadingCtrl
      .create({
        message: 'Creating place...'
      })
      .then(loadingElement => {
        loadingElement.present();

        this.placesService
          .addPlace({
            // Data
            title: this.offerForm.value.titleFormCtrl,
            description: this.offerForm.value.desctriptionFormCtrl,
            price: this.offerForm.value.priceFormCtrl,
            dateFrom: this.offerForm.value.dateFromFormCtrl,
            dateTo: this.offerForm.value.dateToFormCtrl,
            location: this.offerForm.value.locationFormCtrl
          })
          .subscribe(() => {
            loadingElement.dismiss();
            this.offerForm.reset();
            this.router.navigate(['/places/tabs/offers']);
          });
      });
  }

  onLocationPicked(location: PlaceLocation) {
    this.offerForm.patchValue({ locationFormCtrl: location }); // ControlName: Value
  }

  onImagePicked(imageData: string | File) {
    let imageFile;

    if (typeof imageData === 'string') {
      try {

        // console.log(imageData.replace('data:image/png;base64,', ''));
        // return;

        imageFile = base64toBlob(
          imageData.replace('data:image/png;base64,', ''),
          'image/png' // remove base64 prefix
        );
      } catch (e) {
        console.log(e);
        return;
      }
    } else {
      imageFile = imageData;
    }

    console.log('***', this.offerForm);

    this.offerForm.patchValue({ imageFormCntrl: imageFile }); // ControlName: Value

  }
}
