import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Place } from './../../place.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { PlacesService } from '../../places.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit, OnDestroy {
  place: Place;
  placeId: string;
  editOfferForm: FormGroup;
  private placeSubscription: Subscription;
  isLoading = false;

  constructor(
    private route: ActivatedRoute,
    private navCntrl: NavController,
    private placeService: PlacesService,
    private router: Router,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe( paramMap => {
      if (!paramMap.has('placeId')) {
        this.navCntrl.navigateBack('/places/tabs/offers');
        return;
      }
      this.placeId = paramMap.get('placeId');
      // this.place = this.placeService.getPlace(paramMap.get('placeId'));
      this.isLoading = true;
      this.placeSubscription = this.placeService.getPlace(paramMap.get('placeId')).subscribe(
        place => {
          this.place = place;

          this.editOfferForm = new FormGroup({
            titleFormCtrl: new FormControl(this.place.title, {
              updateOn: 'blur',
              validators: [Validators.required]
            }),
            desctriptionFormCtrl: new FormControl(this.place.description, {
              updateOn: 'blur',
              validators: [Validators.required, Validators.maxLength(180)] // Maximum length allowed
            })
          });
          this.isLoading = false;
        }, error => {
          this.alertController.create({
            header: 'An errror occured!',
            message: 'Place could not be fetched. Please try again later',
            buttons: [
              {
                text: 'Okay',
                handler: () => {
                  this.router.navigateByUrl('/places/tabs/offers');
                }
              }
            ]
          }).then(alertElement => {
            alertElement.present();
          });
        }
      );
    });
  }

  ngOnDestroy() {
    if (this.placeSubscription) {
      this.placeSubscription.unsubscribe();
    }
  }

  onUpdateOffer() {
    if (!this.editOfferForm.valid) {
      return;
    }

    // console.log(this.editOfferForm);

    this.loadingController.create({
      message: 'Updating place...'
    }).then(
      loadingElement => {
        loadingElement.present();

        this.placeService.updateOffer({
          Id: this.place.id,
          title: this.editOfferForm.value.titleFormCtrl,
          description: this.editOfferForm.value.desctriptionFormCtrl
        }).subscribe(() => {
          loadingElement.dismiss();
          this.editOfferForm.reset();
          this.router.navigate(['/places/tabs/offers']);
        });
      }
    );

  }


}
