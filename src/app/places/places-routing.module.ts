import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlacesPage } from './places.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs/discover', // -places/tabs/discover
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    component: PlacesPage,
    children: [
      {
        path: '',
        redirectTo: 'discover', // -places/tabs/discover
        pathMatch: 'full'
      },
      {
        path: 'discover', // Need to match the button tab name
        loadChildren: () => import('./discover/discover.module').then( m => m.DiscoverPageModule)
        // children: [
        //   {
        //     path: '',
        //     loadChildren: () => import('./discover/discover.module').then( m => m.DiscoverPageModule)
        //   },
        //   {
        //     path: ':placeId',
        //     loadChildren: () => import('./discover/place-detail/place-detail.module').then( m => m.PlaceDetailPageModule)
        //   }
        // ]
      },
      {
        path: 'offers', // Need to match the button tab name
        loadChildren: () => import('./offers/offers.module').then( m => m.OffersPageModule),
        // children: [
        //   // HARD CODED ROUTES must og first before dynamic(segments) paths
        //   {
        //     path: '',
        //     loadChildren: () => import('./offers/offers.module').then( m => m.OffersPageModule)
        //   },
        //   {
        //     path: 'new',
        //     loadChildren: () => import('./offers/new-offer/new-offer.module').then( m => m.NewOfferPageModule)
        //   },
        //   {
        //     path: 'edit/:placeId', // static + dynamic segment
        //     loadChildren: () => import('./offers/edit-offer/edit-offer.module').then( m => m.EditOfferPageModule)
        //   },
        //   {
        //     path: ':placeId', // static + dynamic segment
        //     loadChildren: () => import('./offers/offer-bookings/offer-bookings.module').then( m => m.OfferBookingsPageModule)
        //   }
        // ]
      }
    ]
  },

  /**
   * Directly changes page it has no parent. Where as
   * having children will force to get inside of its parent
   */
  // {
  //   path: 'discover',
  //   loadChildren: () => import('./discover/discover.module').then( m => m.DiscoverPageModule)
  // },
  // {
  //   path: 'offers',
  //   loadChildren: () => import('./offers/offers.module').then( m => m.OffersPageModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlacesPageRoutingModule {}
