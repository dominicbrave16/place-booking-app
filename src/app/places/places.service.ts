import { PlaceLocation } from './location.model';
import { AuthService } from './../auth/auth.service';
import { Place } from './place.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { take, map, tap, delay, switchMap } from 'rxjs/operators'; // filter - narrow down an array
import { HttpClient } from '@angular/common/http';

interface PlaceData {
  availableFrom: string;
  availableTo: string;
  description: string;
  imageUrl: string;
  price: number;
  title: string;
  userId: string;
  location: PlaceLocation;
}

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  // Event Emitter, emitts most recent data point
  private _places = new BehaviorSubject<Place[]>([
    // new Place(
    //   'p1',
    //   'Maroon Bells, USA',
    //   'Located only about 10 miles from Aspen, Colorado, the Maroon Bells are two 14,000-foot peaks in the Elk Mountains that are reflected in crystal-clear Maroon Lake, snuggled in a glacial valley. They are the crown jewels of the Rocky Mountains and by far one of the most photographed scenes in the country.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp5-vQD-M37eTiR-5nAXW1abzfVqv97KiKhaS3nqlyI_GYr4FshQ&s',
    //   5000,
    //   new Date('2020-01-01'),
    //   new Date('2020-01-31'),
    //   'asd'
    // ),
    // new Place(
    //   'p2',
    //   'Grand Canyon, USA',
    //   'The Grand Canyon is a steep, 1-mile-deep, and up to 18-mile-wide gash in the fabric of the world, an immense gorge carved by the Colorado River over the last 5,000 years. Its sheer size is breathtaking and although you can see only a small portion of it even from the best vantage point, its geology and its age fire the imagination. The layers of colorful rock show the passage of time and some of the rocks at the bottom are 1,8 billion years old.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsb01Jo_8deA021xWybXMyD3r5-6GjZTOIAIu2CH7IyDyKUx8g&s',
    //   3000,
    //   new Date('2020-02-01'),
    //   new Date('2020-06-15'),
    //   'zxc'
    // ),
    // new Place(
    //   'p3',
    //   'Blue Ridge Mountains, USA',
    //   'Located in the eastern United States and part of the massive Appalachians, the Blue Ridge Mountains stretch from their southernmost end in Georgia all the way northward to Pennsylvania.',
    //   'https://img.theculturetrip.com/768x432/wp-content/uploads/2016/02/Iguazu-Falls.jpg',
    //   3000,
    //   new Date('2020-05-01'),
    //   new Date('2020-06-04'),
    //   'asd'
    // ),
    // new Place(
    //   'p1',
    //   'Maroon Bells, USA',
    //   'Located only about 10 miles from Aspen, Colorado, the Maroon Bells are two 14,000-foot peaks in the Elk Mountains that are reflected in crystal-clear Maroon Lake, snuggled in a glacial valley. They are the crown jewels of the Rocky Mountains and by far one of the most photographed scenes in the country.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp5-vQD-M37eTiR-5nAXW1abzfVqv97KiKhaS3nqlyI_GYr4FshQ&s',
    //   5000,
    //   new Date('2020-01-01'),
    //   new Date('2020-01-31'),
    //   '1'
    // ),
    // new Place(
    //   'p2',
    //   'Grand Canyon, USA',
    //   'The Grand Canyon is a steep, 1-mile-deep, and up to 18-mile-wide gash in the fabric of the world, an immense gorge carved by the Colorado River over the last 5,000 years. Its sheer size is breathtaking and although you can see only a small portion of it even from the best vantage point, its geology and its age fire the imagination. The layers of colorful rock show the passage of time and some of the rocks at the bottom are 1,8 billion years old.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsb01Jo_8deA021xWybXMyD3r5-6GjZTOIAIu2CH7IyDyKUx8g&s',
    //   3000,
    //   new Date('2020-02-01'),
    //   new Date('2020-06-15'),
    //   '1'
    // ),
    // new Place(
    //   'p3',
    //   'Blue Ridge Mountains, USA',
    //   'Located in the eastern United States and part of the massive Appalachians, the Blue Ridge Mountains stretch from their southernmost end in Georgia all the way northward to Pennsylvania.',
    //   'https://img.theculturetrip.com/768x432/wp-content/uploads/2016/02/Iguazu-Falls.jpg',
    //   3000,
    //   new Date('2020-05-01'),
    //   new Date('2020-06-04'),
    //   '2'
    // ),
    // new Place(
    //   'p1',
    //   'Maroon Bells, USA',
    //   'Located only about 10 miles from Aspen, Colorado, the Maroon Bells are two 14,000-foot peaks in the Elk Mountains that are reflected in crystal-clear Maroon Lake, snuggled in a glacial valley. They are the crown jewels of the Rocky Mountains and by far one of the most photographed scenes in the country.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp5-vQD-M37eTiR-5nAXW1abzfVqv97KiKhaS3nqlyI_GYr4FshQ&s',
    //   5000,
    //   new Date('2020-01-01'),
    //   new Date('2020-01-31'),
    //   '2'
    // ),
    // new Place(
    //   'p2',
    //   'Grand Canyon, USA',
    //   'The Grand Canyon is a steep, 1-mile-deep, and up to 18-mile-wide gash in the fabric of the world, an immense gorge carved by the Colorado River over the last 5,000 years. Its sheer size is breathtaking and although you can see only a small portion of it even from the best vantage point, its geology and its age fire the imagination. The layers of colorful rock show the passage of time and some of the rocks at the bottom are 1,8 billion years old.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsb01Jo_8deA021xWybXMyD3r5-6GjZTOIAIu2CH7IyDyKUx8g&s',
    //   3000,
    //   new Date('2020-02-01'),
    //   new Date('2020-06-15'),
    //   '1'
    // ),
    // new Place(
    //   'p3',
    //   'Blue Ridge Mountains, USA',
    //   'Located in the eastern United States and part of the massive Appalachians, the Blue Ridge Mountains stretch from their southernmost end in Georgia all the way northward to Pennsylvania.',
    //   'https://img.theculturetrip.com/768x432/wp-content/uploads/2016/02/Iguazu-Falls.jpg',
    //   3000,
    //   new Date('2020-05-01'),
    //   new Date('2020-06-04'),
    //   '1'
    // ),
    // new Place(
    //   'p1',
    //   'Maroon Bells, USA',
    //   'Located only about 10 miles from Aspen, Colorado, the Maroon Bells are two 14,000-foot peaks in the Elk Mountains that are reflected in crystal-clear Maroon Lake, snuggled in a glacial valley. They are the crown jewels of the Rocky Mountains and by far one of the most photographed scenes in the country.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp5-vQD-M37eTiR-5nAXW1abzfVqv97KiKhaS3nqlyI_GYr4FshQ&s',
    //   5000,
    //   new Date('2020-01-01'),
    //   new Date('2020-01-31'),
    //   '1'
    // ),
    // new Place(
    //   'p2',
    //   'Grand Canyon, USA',
    //   'The Grand Canyon is a steep, 1-mile-deep, and up to 18-mile-wide gash in the fabric of the world, an immense gorge carved by the Colorado River over the last 5,000 years. Its sheer size is breathtaking and although you can see only a small portion of it even from the best vantage point, its geology and its age fire the imagination. The layers of colorful rock show the passage of time and some of the rocks at the bottom are 1,8 billion years old.',
    //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsb01Jo_8deA021xWybXMyD3r5-6GjZTOIAIu2CH7IyDyKUx8g&s',
    //   3000,
    //   new Date('2020-02-01'),
    //   new Date('2020-06-15'),
    //   '2'
    // ),
    // new Place(
    //   'p3',
    //   'Blue Ridge Mountains, USA',
    //   'Located in the eastern United States and part of the massive Appalachians, the Blue Ridge Mountains stretch from their southernmost end in Georgia all the way northward to Pennsylvania.',
    //   'https://img.theculturetrip.com/768x432/wp-content/uploads/2016/02/Iguazu-Falls.jpg',
    //   3000,
    //   new Date('2020-05-01'),
    //   new Date('2020-06-04'),
    //   '3'
    // ),
  ]);

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient
  ) {}

  get places() {
    // return [...this._places];
    return this._places.asObservable(); // Subscribable objects
  }

  getPlace(placeId) {
    // return {...this._places.find(place => ( place.id === placeId ))};

    // return this.places.pipe(
    //   take(1),
    //   map(places => {
    //   return {...places.find(place => ( place.id === placeId ))};
    // }));

    // Fetch single place from server
    return this.httpClient
      .get<PlaceData>(
        `https://ionic-angular-4a9f3.firebaseio.com/offered-places/${placeId}.json`
      )
      .pipe(
        // Map constructing object...
        map(placeData => {
          return new Place(
            placeId,
            placeData.title,
            placeData.description,
            placeData.imageUrl,
            placeData.price,
            new Date(placeData.availableFrom),
            new Date(placeData.availableTo),
            placeData.userId,
            placeData.location
          );
        })
      );
  }

  addPlace(place: {
    title: string;
    description: string;
    price: number;
    dateFrom: Date;
    dateTo: Date;
    location: PlaceLocation;
  }) {
    // console.log(place.location);
    let generatedId: string;
    let newPlace: Place;
    return this.authService.userId
      .pipe(
        take(1),
        switchMap(userId => {
          if (!userId) {
            throw new Error('User not Found!');
          }
          newPlace = new Place(
            Math.random().toString(),
            place.title,
            place.description,
            'https://img.theculturetrip.com/768x432/wp-content/uploads/2016/02/Iguazu-Falls.jpg',
            place.price,
            place.dateFrom,
            place.dateTo,
            userId.toString(),
            place.location
          );
          return this.httpClient.post<{ name: string }>(
            'https://ionic-angular-4a9f3.firebaseio.com/offered-places.json',
            {
              ...newPlace,
              id: null
            }
          );
        })
      )
      .pipe(
        // takes the result of observable chain
        switchMap(resData => {
          generatedId = resData.name;
          return this.places;
        }),
        take(1),
        tap(places => {
          newPlace.id = generatedId;
          this._places.next(places.concat(newPlace));
        })
      );
    // this._places.push(newPlace);

    // take(1) - current list only, cancels future subscription
    // this.places.pipe(take(1)).subscribe((places) => {
    //   // this._places.next(places.concat(newPlace));

    //   // Faking loading...
    //   setTimeout(() => {
    //     this._places.next(places.concat(newPlace));
    //   }, 1000);
    // });

    // return this.places.pipe(take(1), delay(1000), tap(places => {
    //     this._places.next(places.concat(newPlace)); // Faking loading...
    // }));

    // console.log(newPlace);
  }

  fetchPlaces() {
    return this.httpClient
      .get<{ [key: string]: PlaceData }>(
        'https://ionic-angular-4a9f3.firebaseio.com/offered-places.json'
      )
      .pipe(
        map(resData => {
          const places = [];
          for (const key in resData) {
            if (resData.hasOwnProperty(key)) {
              places.push(
                new Place(
                  key,
                  resData[key].title,
                  resData[key].description,
                  resData[key].imageUrl,
                  resData[key].price,
                  new Date(resData[key].availableFrom),
                  new Date(resData[key].availableTo),
                  resData[key].userId,
                  resData[key].location
                )
              );
            }
          }
          return places;
          // return []; // Empty Data
        }),
        tap(places => {
          this._places.next(places);
        })
      );

    // map - takes the respone of the observable (non observable)
    // switchmap - observabe
  }

  updateOffer(place: { Id: string; title: string; description: string }) {
    // Update server
    let updatedPlaces: Place[];

    return this.places.pipe(
      take(1),
      switchMap(places => {
        if (!places || places.length <= 0) {
          return this.fetchPlaces();
        } else {
          return of(places); // Transform array into observable
        }
      }),
      switchMap(places => {
        const updatedPlaceIndex = places.findIndex(pl => pl.id === place.Id);
        updatedPlaces = [...places];
        const oldPlace = updatedPlaces[updatedPlaceIndex];
        updatedPlaces[updatedPlaceIndex] = new Place(
          oldPlace.id,
          place.title, // Paramater
          place.description, // Parameter
          oldPlace.imageUrl,
          oldPlace.price,
          oldPlace.availableFrom,
          oldPlace.availableTo,
          oldPlace.userId,
          oldPlace.location
        );
        return this.httpClient.put(
          `https://ionic-angular-4a9f3.firebaseio.com/offered-places/${place.Id}.json`,
          { ...updatedPlaces[updatedPlaceIndex], id: null } // remove ID from storing into firebase
        );
      }),
      tap(() => {
        // Update places local and emit the new list of updated places
        this._places.next(updatedPlaces);
      })
    );

    // return subscription so that it can be manipulated in the specific page...
    // take(1) - latest snapshot, not including future updates
    // tap - get in touch with the value...
    // return this.places.pipe(
    //   take(1),
    //   delay(1000),
    //   tap(places => {
    //   const updatedPlaceIndex = places.findIndex(pl => pl.id === place.Id);
    //   const updatedPlaces = [...places];
    //   const oldPlace = updatedPlaces[updatedPlaceIndex];
    //   updatedPlaces[updatedPlaceIndex] = new Place(
    //     oldPlace.id,
    //     place.title, // Paramater
    //     place.description, // Parameter
    //     oldPlace.imageUrl,
    //     oldPlace.price,
    //     oldPlace.availableFrom,
    //     oldPlace.availableTo,
    //     oldPlace.userId
    //   );
    //   this._places.next(updatedPlaces);
    // }));
  }

  uploadImage(image: File) {
    // ...
  }
}
