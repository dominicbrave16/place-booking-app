import { switchMap } from 'rxjs/operators';
import { MapModalComponent } from './../../../shared/modals/map-modal/map-modal.component';
import { AuthService } from './../../../auth/auth.service';
import { BookingService } from './../../../bookings/booking.service';
import { CreateBookingComponent } from './../../../bookings/create-booking/create-booking.component';
import { PlacesService } from './../../places.service';
import { Place } from './../../place.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  NavController,
  ModalController,
  ActionSheetController,
  LoadingController,
  AlertController
} from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss']
})
export class PlaceDetailPage implements OnInit, OnDestroy {
  private placeSubscription: Subscription;
  isBookable = false;
  place: Place;
  isLoading = false;

  constructor(
    private router: Router,
    private navCntrl: NavController,
    private actvRoute: ActivatedRoute,
    private placeService: PlacesService,
    private modalController: ModalController,
    private actionSheet: ActionSheetController,
    private bookingService: BookingService,
    private loadingController: LoadingController,
    private authService: AuthService,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.actvRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('placeId')) {
        this.navCntrl.navigateBack('/places/tabs/discover');
        return;
      }
      // this.place = this.placeService.getPlace(paramMap.get('placeId'));

      this.isLoading = true;
      let fetchedUserId: string | boolean;
      this.authService.userId
        .pipe(
          switchMap(userId => {
            if (!userId) {
              throw new Error('User not Found!');
            }

            fetchedUserId = userId;
            return this.placeService.getPlace(paramMap.get('placeId'));
          })
        )
        .subscribe(
          place => {
            this.place = place;
            // console.log(place.title, place.userId);
            this.isBookable = place.userId !== fetchedUserId;
            this.isLoading = false;
          },
          error => {
            this.alertController
              .create({
                header: 'An error occured!',
                message: 'Could not load place.',
                buttons: [
                  {
                    text: 'Okay',
                    handler: () => {
                      this.router.navigate(['/places/tabs/discover']);
                    }
                  }
                ]
              })
              .then(alertElement => {
                alertElement.present();
              });
          }
        );
    });
  }

  ngOnDestroy(): void {
    if (this.placeSubscription) {
      this.placeSubscription.unsubscribe();
    }
  }

  onBookPlace() {
    // this.router.navigateByUrl('/places/tabs/discover');
    // this.router.navigate(['places','tabs','discover']);
    // this.navCntrl.navigateBack(['places', 'tabs', 'discover']); // Much better
    // this.navCntrl.pop(); // Unreliable

    this.actionSheet
      .create({
        header: 'Choose an Action',
        buttons: [
          {
            text: 'Select Date',
            handler: () => {
              this.openBookingModal('select');
            }
          },
          {
            text: 'Random Date',
            handler: () => {
              this.openBookingModal('random');
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      })
      .then(actionSheetElement => {
        actionSheetElement.present();
      });
  }

  openBookingModal(mode: 'select' | 'random') {
    // console.log(mode);

    // Opens modal
    this.modalController
      .create({
        id: 'bookPlaceModal',
        component: CreateBookingComponent,
        componentProps: { selectedPlace: this.place, selectedMode: mode } // Passed into @Input of component
      })
      .then(modalElement => {
        modalElement.present();
        return modalElement.onDidDismiss();
      })
      .then(resultData => {
        // console.log(resultData.data, resultData.role);
        if (resultData.role === 'confirm') {
          this.loadingController
            .create({
              message: 'Booking place...'
            })
            .then(loadingElement => {
              loadingElement.present();
              // console.log('Confirmed!');

              const data = resultData.data.bookingData;
              this.bookingService
                .addBooking(
                  this.place.id,
                  this.place.title,
                  this.place.imageUrl,
                  data.firstName,
                  data.lastName,
                  data.guestCount,
                  data.startDate,
                  data.endDate
                )
                .subscribe(() => {
                  loadingElement.dismiss();
                });
            });
        }
      });
  }

  onShowFullMap() {
    this.modalController
      .create({
        id: 'mapModal',
        component: MapModalComponent,
        componentProps: {
          center: {
            lat: this.place.location.lat,
            lng: this.place.location.lng
          },
          selectable: false,
          closeButtonText: 'Close',
          title: this.place.location.address
        }
      })
      .then(modalElement => {
        modalElement.present();
      });
  }
}
