import { take } from 'rxjs/operators';
import { AuthService } from './../../auth/auth.service';
import { Place } from './../place.model';
import { PlacesService } from './../places.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SegmentChangeEventDetail } from '@ionic/core';
import { MenuController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss']
})
export class DiscoverPage implements OnInit, OnDestroy {
  loadedPlaces: Place[];
  listedLoadedPlaces: Place[];
  relevantPlaces: Place[];
  private placesSubscription: Subscription;
  isLoading = false;

  constructor(
    private placesService: PlacesService,
    private menuControl: MenuController,
    private authService: AuthService
  ) {}

  ngOnInit() {
    // this.loadedPlaces = this.placesService.places;
    // this.listedLoadedPlaces = this.placesService.places.slice(1);

    this.placesSubscription = this.placesService.places.subscribe(places => {
      this.loadedPlaces = places;
      this.relevantPlaces = this.loadedPlaces;
      this.listedLoadedPlaces = this.relevantPlaces.slice(1);
    });
  }

  ionViewWillEnter() {
    if (this.loadedPlaces.length <= 0) {
      this.isLoading = true;
      this.placesService.fetchPlaces().subscribe(() => {
        this.isLoading = false;
      });
    }
  }

  ngOnDestroy(): void {
    if (this.placesSubscription) {
      this.placesSubscription.unsubscribe();
    }
  }

  onOpenMenu() {
    // this.menuControl.toggle('side-bar-menu');
  }

  // Segement button
  onFilterUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
    this.authService.userId.pipe(take(1)).subscribe(userId => {
      // console.log(event.detail);
      if (event.detail.value === 'all') {
        this.relevantPlaces = this.loadedPlaces;
        this.listedLoadedPlaces = this.relevantPlaces.slice(1);
      } else {
        this.relevantPlaces = this.loadedPlaces.filter(place => {
          // console.log(place.userId, this.authService.userId);
          return place.userId !== userId;
        });
        this.listedLoadedPlaces = this.relevantPlaces.slice(1);
      }
    });
  }
}
