import { Place } from './../../places/place.model';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.scss'],
})
export class CreateBookingComponent implements OnInit {

  @Input() selectedPlace: Place;
  @Input() selectedMode: 'select' | 'random'; // Passed from component prop
  @ViewChild('bookingForm', {static: true}) bookingForm: NgForm; // Access elements from the template named with #

  startDate: string;
  endDate: string;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
    const availableFrom = new Date(this.selectedPlace.availableFrom);
    const availableTo = new Date(this.selectedPlace.availableTo);

    if (this.selectedMode === 'random') {
      this.startDate = new Date( availableFrom.getTime() + Math.random() * (availableTo.getTime() - (4 * 24 * 60 * 60 * 1000) - availableFrom.getTime())).toISOString();
      this.endDate = new Date(new Date(this.startDate).getTime() + Math.random() * (new Date(this.startDate)).getTime() + (3 * 24 * 60 * 60 * 1000) - new Date(this.startDate).getTime()).toISOString();
    }

  }

  onCancel() {
    this.modalController.dismiss(null, 'cancel', 'bookPlaceModal');
  }

  onBookPlace() {

    // Prevent from frontend override
    if (!this.bookingForm.valid || !this.datesValid) {
      console.log('Invalid Data...');
      return;
    }

    this.modalController.dismiss({
      message: 'This is a dummy message',
      myMessage: 'Hello',
      bookingData: {
        firstName: this.bookingForm.value.firstName,
        lastName: this.bookingForm.value.lastName,
        guestCount: +this.bookingForm.value.guestCount,
        startDate: new Date(this.bookingForm.value.dateFrom),
        endDate: new Date(this.bookingForm.value.dateTo),
      }
    }, 'confirm', 'bookPlaceModal');
  }

  datesValid() {

    const startDate: Date = new Date(this.bookingForm.value.dateFrom);
    const endDate: Date = new Date(this.bookingForm.value.dateTo);

    return endDate > startDate;

  }


}
