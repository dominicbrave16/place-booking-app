import { take, tap, delay, switchMap, map } from 'rxjs/operators';
import { AuthService } from './../auth/auth.service';
import { Booking } from './booking.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

interface BookingData {
  bookedFrom: string;
  bookedTo: string;
  firstName: string;
  guestNumber: string;
  lastName: string;
  placeId: string;
  placeImage: string;
  placeTitle: string;
  userId: string;
}

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  // private _bookings: Booking[] = [
  //   {
  //     id: 'xyz',
  //     placeId: 'p1',
  //     placeTitle: 'Maroon Bells, USA',
  //     guestNumber: 2,
  //     userId: 'abc'

  //   },
  //   {
  //     id: 'xyzxxxsa',
  //     placeId: 'p2',
  //     placeTitle: 'Grand Canyon, USA',
  //     guestNumber: 4,
  //     userId: 'defe'

  //   },
  //   {
  //     id: 'dasd',
  //     placeId: 'p3',
  //     placeTitle: 'Blue Ridge Mountains, USA',
  //     guestNumber: 1,
  //     userId: 'abcdef'

  //   }
  // ];
  private _bookings = new BehaviorSubject<Booking[]>([]);

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient
  ) {}

  get bookings() {
    return this._bookings.asObservable();
  }

  addBooking(
    placeId: string,
    placeTitle: string,
    placeImage: string,
    firstName: string,
    lastName: string,
    guestNumber: number,
    dateFrom: Date,
    dateTo: Date
  ) {
    let generatedId: string;
    let newBooking: Booking;
    return this.authService.userId.pipe(
      take(1),
      switchMap(userId => {
        if (!userId) {
          throw new Error('No user id found!!');
        }

        newBooking = new Booking(
          Math.random().toString(),
          placeId,
          userId.toString(),
          placeTitle,
          placeImage,
          firstName,
          lastName,
          guestNumber,
          dateFrom,
          dateTo
        );

        return this.httpClient.post<{ name: string }>(
          'https://ionic-angular-4a9f3.firebaseio.com/bookings.json',
          { ...newBooking, id: null }
        );
      }),
      switchMap(resultData => {
        generatedId = resultData.name;
        // console.log(resultData);
        return this.bookings;
      }),
      take(1),
      tap(bookings => {
        newBooking.id = generatedId;
        this._bookings.next(bookings.concat(newBooking));
      })
    );

    // Without backend...
    // return this.bookings.pipe(
    //   take(1),
    //   delay(1000),
    //   tap(
    //     bookings => {
    //       this._bookings.next(bookings.concat(newBooking));
    //     }
    // ));
  }

  fetchBookings() {
    // Firebase must set rules in order to allow indexing
    return this.authService.userId.pipe(
      switchMap(userId => {
        if (!userId) {
          throw new Error('User not found!');
        }
        return this.httpClient.get<{ [key: string]: BookingData }>(
          `https://ionic-angular-4a9f3.firebaseio.com/bookings.json?orderBy="userId"&equalTo="${userId}"`
        );
      }),
      map(resData => {
        // Map transform data
        const bookings = [];
        for (const key in resData) {
          if (resData.hasOwnProperty(key)) {
            bookings.push(
              new Booking(
                key,
                resData[key].placeId,
                resData[key].userId,
                resData[key].placeTitle,
                resData[key].placeImage,
                resData[key].firstName,
                resData[key].lastName,
                +resData[key].guestNumber,
                new Date(resData[key].bookedFrom),
                new Date(resData[key].bookedTo)
              )
            );
          }
        }
        return bookings;
      }),
      tap(bookings => {
        this._bookings.next(bookings);
      })
    );
  }

  cancelBooking(bookingId: string) {
    return this.httpClient
      .delete(
        `https://ionic-angular-4a9f3.firebaseio.com/bookings/${bookingId}.json`
      )
      .pipe(
        switchMap(() => {
          return this.bookings; // -> return in the next line as bookings
        }),
        take(1), // Listen only for single data change
        tap(bookings => {
          this._bookings.next(bookings.filter(b => b.id !== bookingId));
        })
      );

    // Without backend
    // return this.bookings.pipe(
    //   take(1),
    //   delay(1000),
    //   tap(
    //     bookings => {
    //       this._bookings.next(bookings.filter(b => b.id !== bookingId)); // returns true if not equal -> true is keep | false -> filtered out
    //     }
    // ));
  }
}
