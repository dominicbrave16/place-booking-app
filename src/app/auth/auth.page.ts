import { Observable } from 'rxjs';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService, AuthResponseData } from './auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit {
  // isLoading = false;
  isLoginMode = true;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  // onLogin() {
  //   this.authService.onLogin();
  //   // Fake loading
  //   // this.isLoading = true;
  //   // setTimeout(() => {
  //   //   this.isLoading = false;
  //   //   this.router.navigateByUrl('/places/tabs/discover');
  //   // }, 1500);

  //   this.loadingController.create({
  //     keyboardClose: true,
  //     message: 'Logging in...',
  //   }).then(
  //     loadingElement => {
  //       loadingElement.present();
  //       setTimeout(() => {
  //         loadingElement.dismiss();
  //         this.router.navigateByUrl('/places/tabs/discover');
  //       }, 1500);
  //     }
  //   );
  // }

  authenticate(email: string, password: string) {
    // this.authService.onLogin();
    // Fake loading
    // this.isLoading = true;
    // setTimeout(() => {
    //   this.isLoading = false;
    //   this.router.navigateByUrl('/places/tabs/discover');
    // }, 1500);

    this.loadingController
      .create({
        keyboardClose: true,
        message: `${this.isLoginMode ? 'Logging' : 'Signing'} in...`
      })
      .then(loadingElement => {
        loadingElement.present();

        let authObservable: Observable<AuthResponseData>;

        if (this.isLoginMode) {
          authObservable = this.authService.onLogin(email, password);
        } else {
          authObservable = this.authService.signupUser(email, password);
        }

        authObservable.subscribe(
          responseData => {
            console.log(responseData);
            loadingElement.dismiss();
            this.router.navigateByUrl('/places/tabs/discover');
          },
          errorResponse => {
            loadingElement.dismiss();

            const errMessage = errorResponse.error.error.message;
            let message = 'Could not sign you up, please try again.';

            // console.log(errorResponse, errMessage);
            // if (errMessage === 'EMAIL_EXISTS') {
            //   message = 'This email address already exist!';
            // }

            switch (errMessage) {
              case 'EMAIL_EXISTS':
                message = 'This email address already exist!';
                break;
              case 'EMAIL_NOT_FOUND':
                message = 'Email address could not be found!';
                break;
              case 'INVALID_PASSWORD':
                message = 'The password is incorrect!';
                break;
            }

            this.showAlert(message);
          }
        );

        // setTimeout(() => {
        //   loadingElement.dismiss();
        //   this.router.navigateByUrl('/places/tabs/discover');
        // }, 1500);
      });
  }

  onSubmit(loginForm: NgForm) {
    // console.log(loginForm);
    if (!loginForm.valid) {
      return;
    }

    const email = loginForm.value.inputEmail;
    const password = loginForm.value.inputPassword;

    // console.log(email, password);

    // if (this.isLoginMode) {
    //   // Send a request to login servers
    // } else {
    //   // this.authService
    //   //   .signupUser(email, password)
    //   //   .subscribe(responseData => {
    //   //     console.log(responseData);
    //   //   });
    // }
    this.authenticate(email, password);
  }

  onSwitchAuthMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  private showAlert(message: string) {
    this.alertController
      .create({
        header: 'Authentication Failed!',
        message,
        buttons: ['Okay']
      })
      .then(alertElement => {
        alertElement.present();
      });
  }
}
