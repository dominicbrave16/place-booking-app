import { take, tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {
  CanLoad,
  Route,
  UrlSegment,
  Router,
  CanActivate
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
// export class AuthGuard implements CanActivate {

//   canActivate(route: import('@angular/router').ActivatedRouteSnapshot, state: import('@angular/router').RouterStateSnapshot): boolean | import('@angular/router').UrlTree | Observable<boolean | import('@angular/router').UrlTree> | Promise<boolean | import('@angular/router').UrlTree> {
//     if (!this.authService.userIsAuthenticated) {
//       this.router.navigateByUrl('/auth');
//     }

//     return this.authService.userIsAuthenticated;
//   }

//   constructor(private authService: AuthService, private router: Router) {}
// }
export class AuthGuard implements CanLoad {
  // Guards loading  - lazy loading...
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    // if (!this.authService.userIsAuthenticated) {
    //   this.router.navigateByUrl('/auth');
    // }

    return this.authService.userIsAuthenticated.pipe(
      take(1),
      tap(isAuthenticated => {
        if (!isAuthenticated) {
          this.router.navigateByUrl('/auth');
        }
      })
    );
  }

  constructor(private authService: AuthService, private router: Router) {}
}
