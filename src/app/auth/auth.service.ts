import { map, tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user.model';

export interface AuthResponseData {
  idToken: string;
  email: string;
  refresh: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private _userIsAuthenticated = false; // Debug mode -> _userIsAuthenticated = true
  // private _userId = null;
  // private _token = new BehaviorSubject<string>(); // Must be a behaviour subject

  private _user = new BehaviorSubject<User>(null);

  constructor(private httpClient: HttpClient) {}

  get userIsAuthenticated() {
    // return this._userIsAuthenticated;
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return !!user.token;
        } else {
          return false;
        }
      })
    ); // !! convers into boolean from string.
  }

  get userId() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.id;
        } else {
          return false;
        }
      })
    );
  }

  signupUser(email: string, password: string) {
    return this.httpClient
      .post<AuthResponseData>(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA2eB3aEZfqFJuL3ojpMXx18AY-Zy-0Jjs',
        { email, password, returnSecureToken: true }
      )
      .pipe(tap(this.setUserData.bind(this)));
  }

  onLogin(email: string, password: string) {
    // this._userIsAuthenticated = true;
    return this.httpClient
      .post<AuthResponseData>(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA2eB3aEZfqFJuL3ojpMXx18AY-Zy-0Jjs',
        { email, password, returnSecureToken: true }
      )
      .pipe(tap(this.setUserData.bind(this)));
  }

  private setUserData(userData: AuthResponseData) {
    const expirationTime = new Date(
      new Date().getTime() + +userData.expiresIn * 1000
    );
    this._user.next(
      new User(
        userData.localId,
        userData.email,
        userData.idToken,
        expirationTime
      )
    );
  }

  onLogout() {
    this._user.next(null);
  }
}
